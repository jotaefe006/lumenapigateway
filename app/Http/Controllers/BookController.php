<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Services\AuthorService;
use App\Services\BookService;
use App\Traits\ApiResponser;
// use Illuminate\Http\Client\Request;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
class BookController extends Controller
{
    use ApiResponser;

    /**
     * the service to  consume the books microservice
     */
    public $bookService;
    /**
     * the service to  consume the books microservice
     */
    public $authorService;

    /**
     * Create a new controller instance.
     * @var BookService
     * @return void
     */
    public function __construct( BookService $bookService, AuthorService $authorService)
    {
        $this->bookService = $bookService;
        $this->authorService = $authorService;
    }

    /**
     * return the list of Books
     * @return Illuminate\Http\Response
     */
    public function index(){

        return $this->successResponse($this->bookService->obtainBooks());

    }
    /**
     * Create one new book
     * @return Illuminate\Http\Response
     */

    public function store(Request $request){

        $this->authorService->obtainAuthor($request->author_id);
        return $this->successResponse($this->bookService->createBook($request->all(), Response::HTTP_CREATED));
    }

    public function show($book){

        return $this->successResponse($this->bookService->obtainBook($book));
    }

    public function update(Request $request,$book){

        return $this->successResponse($this->bookService->editBook($request->all(), $book));
    }

    public function destroy($book){

        return $this->successResponse($this->bookService->deleteBook($book));
    }
}
