<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponser
{

    /**
     * Build success response
     * @param string|array $data
     * @param int $code
     * @return Illuminate\Http\JsonResponse
     */
    public function successResponse($data, $statusCode = Response::HTTP_OK)
    {
        return response($data, $statusCode)->header('Content-type','application/json');
    }

    /**
     * Build valid response
     * @param string|array $data
     * @param int $code
     * @return Illuminate\Http\JsonResponse
     */
    public function validResponse($data, $statusCode = Response::HTTP_OK)
    {
        return response()->json(['data'=> $data], $statusCode);
    }


    public function errorResponse($errorMessage, $statusCode)
    {
        return response()->json(['error' => $errorMessage, 'error_code' => $statusCode], $statusCode);
    }

    public function errorMesage($message, $statusCode){
        return response($message, $statusCode)->header('Content-type','application/json');
    }
}
