<?php

namespace App\Services;

use App\Traits\ConsumeExternalServices;

class AuthorService
{
    use ConsumeExternalServices;

    /**
     * The base uri to consume the authors service
     * @var string
     */
    public $baseUri;

    /**
     * The secret to consume the authors service
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.authors.base_uri');
        $this->secret = config('services.authors.secret');
    }

    /**
     * obtain the full list of author from the author service
     * @return string
     */
    public function obtainAuthors(){

        return $this->performRequest('GET','/authors');

    }

    /**
     * Create one author using the author service
     * @return string
     */
    public function createAuthor($data){
        return $this->performRequest('POST','/authors',$data);

    }

    /**
     * obtain one single author from the author service
     * @return string
     */
    public function obtainAuthor($autor){

        return $this->performRequest('GET',"/authors/{$autor}");

    }

    /**
     * Update an instance of author using author service
     * @return string
     */
    public function editAuthor($data,$autor){

        return $this->performRequest('PUT',"/authors/{$autor}",$data);
    }

    /**
     * Delete one single author using author service
     */
    public function deleteAuthor($autor){
        return $this->performRequest('DELETE',"/authors/{$autor}");
    }


}
