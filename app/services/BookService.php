<?php

namespace App\Services;

use App\Traits\ConsumeExternalServices;

class BookService
{
    use ConsumeExternalServices;

    /**
     * The base uri to consume the books service
     * @var string
     */
    public $baseUri;

    /**
     * The secret to consume the books service
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.books.base_uri');
        $this->secret = config('services.books.secret');
    }

    /**
     * obtain the full list of Book from the Book service
     * @return string
     */
    public function obtainBooks(){

        return $this->performRequest('GET','/books');

    }

    /**
     * Create one Book using the Book service
     * @return string
     */
    public function createBook($data){
        return $this->performRequest('POST','/books',$data);

    }

    /**
     * obtain one single Book from the Book service
     * @return string
     */
    public function obtainBook($book){

        return $this->performRequest('GET',"/books/{$book}");

    }

    /**
     * Update an instance of Book using Book service
     * @return string
     */
    public function editBook($data,$book){

        return $this->performRequest('PUT',"/books/{$book}",$data);
    }

    /**
     * Delete one single Book using Book service
     */
    public function deleteBook($book){
        return $this->performRequest('DELETE',"/books/{$book}");
    }
}
