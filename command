php -S localhost:8002 -t .\public

composer require guzzlehttp/guzzle
composer require dusterio/lumen-passport

php artisan passport:install

-----------
this is what's added to bootstrap\app.php

$app->routeMiddleware([
    'client.credentials' => Laravel\Passport\Http\Middleware\CheckClientCredentials::class,
]);
$app->register(Laravel\Passport\PassportServiceProvider::class);
$app->register(Dusterio\LumenPassport\PassportServiceProvider::class);
$app->configure('auth');
here is from config/auth.php (copy from vendor/laravel/lumen-framework/config/auth.php

  'guards' => [
       'api' => [
           'driver' => 'passport',
           'provider' => 'users'
       ],
   ],
---
  'providers' => [
       'users' => [
           'driver' => 'eloquent',
           'model' => App\Models\User_Master::class #state your user model
       ]
   ],
then I execute php artisan passport:client

 Which user ID should the client be assigned to?: # i input my admin ID from user_master
1
 What should we name the client?:
MyClient
 Where should we redirect the request after authorization? # blank

---------------
